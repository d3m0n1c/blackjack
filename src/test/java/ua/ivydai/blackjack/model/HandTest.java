package ua.ivydai.blackjack.model;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestSuite;

public class HandTest extends TestSuite {
    private Hand hand;

    @Before
    public void createNewHand() {
        hand = new Hand();
    }

    @Test
    public void testEmptyHand() {
        assertEquals(0, hand.getOpenedCardsCount());
        assertEquals(0, hand.getClosedCardsCount());
        assertEquals(0, hand.getHandValue());
        assertEquals(Collections.emptyList(), hand.getOpenedCards());
    }

    @Test
    public void testAddingCards() {
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, 1);
        Card openedCard = deck.popNextCard();
        hand.addOpenedCard(openedCard);
        Card closedCard = deck.popNextCard();
        hand.addClosedCard(closedCard);
        assertEquals(1, hand.getOpenedCardsCount());
        assertEquals(1, hand.getClosedCardsCount());
        assertEquals(openedCard.getCardValue().getValue(), hand.getHandValue());
        hand.openNextClosedCard();
        assertEquals(2, hand.getOpenedCardsCount());
        assertEquals(0, hand.getClosedCardsCount());
        assertEquals(openedCard.getCardValue().getValue()
                + closedCard.getCardValue().getValue(), hand.getHandValue());
    }
}
