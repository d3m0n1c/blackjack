package ua.ivydai.blackjack.model;

import akka.actor.ActorRef;

/*******************************************************************************
 * Class container for players sorting according to hand value in decreasing
 * order.
 *
 * @author IVydai
 ******************************************************************************/
public class Player implements Comparable<Player> {
    private final ActorRef player;

    private final Hand playerHand;

    public Player(ActorRef player, Hand playerHand) {
        this.player = player;
        this.playerHand = playerHand;
    }

    public ActorRef getPlayer() {
        return player;
    }

    public Hand getPlayerHand() {
        return playerHand;
    }

    @Override
    public int compareTo(Player o) {
        return o.getPlayerHand().getHandValue()
                - this.getPlayerHand().getHandValue();
    }
}
