package ua.ivydai.blackjack.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import junit.framework.TestSuite;

public class PlayerTest extends TestSuite {
    @Test
    public void testPlayerSorting() {
        final int decksCount = 100;
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, decksCount);
        List<Player> players = new ArrayList<>(deck.size() / decksCount);
        Hand hand = null;
        for (int i = 0; i < deck.size(); i++) {
            if (i % decksCount == 0) {
                hand = new Hand();
                players.add(new Player(null, hand));
            }
            hand.addOpenedCard(deck.popNextCard());
        }
        Collections.sort(players);
        int max = Integer.MAX_VALUE;
        for (Player player : players) {
            int playerHandValue = player.getPlayerHand().getHandValue();
            assertTrue(playerHandValue <= max);
            max = playerHandValue;
        }
    }
}
