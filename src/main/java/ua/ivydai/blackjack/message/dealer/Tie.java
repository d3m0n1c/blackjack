package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

import ua.ivydai.blackjack.model.Hand;

/*******************************************************************************
 * Message from Dealer with dealers hand that equals to player's hand by value.
 *
 * @author IVydai
 ******************************************************************************/
public class Tie implements Serializable {
    private static final long serialVersionUID = -6264628245765864506L;

    private final Hand dealerHand;

    public Tie(Hand dealerHand) {
        this.dealerHand = dealerHand;
    }

    public Hand getDealerHand() {
        return dealerHand;
    }
}
