package ua.ivydai.blackjack;

import ua.ivydai.blackjack.actor.DealerActor;
import akka.actor.ActorSystem;
import akka.actor.Props;

/*******************************************************************************
 * Server starter for dealer player.
 *
 * @author IVydai
 ******************************************************************************/
public class DealerStarter {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("Blackjack");
        system.actorOf(Props.create(DealerActor.class), "dealerActor");
    }
}
