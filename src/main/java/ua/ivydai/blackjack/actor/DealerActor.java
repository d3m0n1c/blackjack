package ua.ivydai.blackjack.actor;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.ivydai.blackjack.message.dealer.*;
import ua.ivydai.blackjack.message.player.*;
import ua.ivydai.blackjack.model.Card;
import ua.ivydai.blackjack.model.Deck;
import ua.ivydai.blackjack.model.DeckType;
import ua.ivydai.blackjack.model.Hand;
import ua.ivydai.blackjack.model.Player;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

/*******************************************************************************
 * Dealer actor that should know all about players and coordinate gaming
 * process.
 *
 * @author IVydai
 ******************************************************************************/
public class DealerActor extends UntypedActor {
    private static final Logger LOG = LoggerFactory.getLogger(DealerActor.class);

    private static final int START_PLAYER_FUNDS = 1000;

    private static final int MIN_PLAYERS_TO_START = 2;

    private static final DeckType DECK_TYPE = DeckType.STANDARD_DECK;

    private static final int DECK_COUNT = 2;

    private final List<ActorRef> connectedPlayers = new LinkedList<>();

    private final Map<ActorRef, Integer> playersBid = new HashMap<>();

    private final Map<ActorRef, Integer> playersMoney = new HashMap<>();

    private final Map<ActorRef, Hand> playersHand = new HashMap<>();

    private Hand dealerHand;

    private Deck deck;

    private int placedBidsCount;

    private List<ActorRef> players;

    private List<Player> orderedPlayers;

    private boolean roundIsInProgress = false;

    public DealerActor() {
        LOG.info("Dealer started");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Join) {
            initializePlayerData();
            getSelf().tell(new StartRound(), ActorRef.noSender());
        } else if (message instanceof StartRound) {
            if (roundIsInProgress) {
                LOG.info("Can't start new round due to round has already been started");
                return;
            } else if (connectedPlayers.size() < MIN_PLAYERS_TO_START) {
                LOG.info("Can't start new round due to lack of players");
                return;
            }
            initializeNewRoundData();
        } else if (message instanceof Bid) {
            Bid input = (Bid) message;
            processPlayerBid(input.getPlayerBid());
            placedBidsCount++;
            getSelf().tell(new CheckBidsCount(), ActorRef.noSender());
        } else if (message instanceof CheckBidsCount) {
            if (players.size() == placedBidsCount) {
                giveCardsForPlayersAndStart();
            }
        } else if (message instanceof LeaveTable) {
            removePlayerFromTable();
        } else if (message instanceof NextPlayerTurn) {
            informNextPlayerAboutHisTurn();
        } else if (message instanceof Hit) {
            processPlayersHit();
        } else if (message instanceof Stand) {
            // simply skip to the next player turn
            getSelf().tell(new NextPlayerTurn(), ActorRef.noSender());
        } else if (message instanceof DealerTurn) {
            processDealerTurn();
            // go to a new round
            roundIsInProgress = false;
            getSelf().tell(new StartRound(), ActorRef.noSender());
        } else {
            LOG.warn("Unhandled message: " + message.toString());
            unhandled(message);
        }
    }

    private void processPlayerBid(int playerBid) {
        final ActorRef sender = getSender();
        int newBidValue = playerBid + playersBid.get(sender);
        playersBid.put(sender, newBidValue);
        int newMoneyValue = playersMoney.get(sender) - playerBid;
        playersMoney.put(sender, newMoneyValue);
    }

    private void processDealerTurn() {
        final ActorRef self = getSelf();
        dealerHand.openNextClosedCard(); // it's only one from the start
        while (dealerHand.getHandValue() <= 16) {
            dealerHand.addOpenedCard(getNextCard());
        }
        int dealerHandValue = dealerHand.getHandValue();
        boolean isDealerBusts = dealerHandValue > 21;
        for (ActorRef player : players) {
            if (isDealerBusts) {
                winForPlayer(self, player);
                continue;
            }
            int playerHandValue = playersHand.get(player).getHandValue();
            if (playerHandValue > dealerHandValue) {
                winForPlayer(self, player);
            } else if (playerHandValue < dealerHandValue) {
                playersBid.put(player, 0);
                player.tell(new Loose(dealerHand), self);
                if (playersMoney.get(player) == 0) {
                    player.tell(new NoMoreMoney(), self);
                    self.tell(new LeaveTable(), player);
                }
            } else {
                player.tell(new Tie(dealerHand), self);
            }
        }
    }

    private void processPlayersHit() {
        final ActorRef sender = getSender();
        final ActorRef self = getSelf();
        Hand playerHand = playersHand.get(sender);
        Card playerCard = getNextCard();
        playerHand.addOpenedCard(playerCard);
        int handValue = playerHand.getHandValue();
        sender.tell(new PlayerCard(playerCard), self);
        if (handValue < 21) {
            sender.tell(new YourTurn(), self);
        } else if (handValue == 21) {
            sender.tell(new Blackjack(), self);
            self.tell(new NextPlayerTurn(), ActorRef.noSender());
        } else {
            sender.tell(new TooManyCards(), self);
            // immediately left the round
            playersBid.put(sender, 0);
            players.remove(sender);
            if (playersMoney.get(sender) == 0) {
                sender.tell(new NoMoreMoney(), self);
                self.tell(new LeaveTable(), sender);
            }
            self.tell(new NextPlayerTurn(), ActorRef.noSender());
        }
    }

    private void removePlayerFromTable() {
        final ActorRef sender = getSender();
        final ActorRef self = getSelf();
        players.remove(sender);
        connectedPlayers.remove(sender);
        playersBid.remove(sender);
        playersMoney.remove(sender);
        playersHand.remove(sender);
        // ActorRef is ready to be terminated
        sender.tell(new OkBye(), self);
        if (players.isEmpty()) {
            roundIsInProgress = false;
            self.tell(new StartRound(), ActorRef.noSender());
        } else {
            self.tell(new CheckBidsCount(), ActorRef.noSender());
        }
    }

    private void giveCardsForPlayersAndStart() {
        final ActorRef self = getSelf();
        for (ActorRef player : players) {
            Card playerCard = getNextCard();
            playersHand.get(player).addOpenedCard(playerCard);
            player.tell(new PlayerCard(playerCard), self);
        }
        Card openCardForDealer = getNextCard();
        dealerHand.addOpenedCard(openCardForDealer);
        for (ActorRef player : players) {
            player.tell(new DealerCard(openCardForDealer), self);
        }
        for (ActorRef player : players) {
            Card playerCard = getNextCard();
            playersHand.get(player).addOpenedCard(playerCard);
            player.tell(new PlayerCard(playerCard), self);
        }
        List<Player> tempList = new LinkedList<>();
        for (ActorRef player : players) {
            tempList.add(new Player(player, playersHand.get(player)));
        }
        Collections.sort(tempList);
        orderedPlayers = tempList;
        self.tell(new NextPlayerTurn(), ActorRef.noSender());
    }

    private void informNextPlayerAboutHisTurn() {
        if (orderedPlayers.isEmpty()) {
            getSelf().tell(new DealerTurn(), ActorRef.noSender());
        } else {
            Player currentPlayer = orderedPlayers.remove(0);
            currentPlayer.getPlayer().tell(new YourTurn(), getSelf());
        }
    }

    private void initializeNewRoundData() {
        roundIsInProgress = true;
        players = new LinkedList<>(connectedPlayers);
        deck = Deck.createDeck(DECK_TYPE, DECK_COUNT);
        dealerHand = new Hand();
        dealerHand.addClosedCard(getNextCard());
        placedBidsCount = 0;
        // this should be sent to each player on round start
        for (ActorRef player : players) {
            playersHand.put(player, new Hand());
            int playerMoney = playersMoney.get(player);
            int playerBid = playersBid.get(player);
            player.tell(new RoundStarted(playerMoney, playerBid), getSelf());
        }
    }

    private void initializePlayerData() {
        final ActorRef sender = getSender();
        connectedPlayers.add(sender);
        playersBid.put(sender, 0);
        playersMoney.put(sender, START_PLAYER_FUNDS);
    }

    private void winForPlayer(ActorRef dealer, ActorRef player) {
        int playerBid = playersBid.get(player);
        playersBid.put(player, 0);
        int playerMoney = playersMoney.get(player);
        playersMoney.put(player, playerMoney + playerBid * 2);
        player.tell(new Win(dealerHand), dealer);
    }

    private Card getNextCard() {
        if (deck.size() == 0) {
            LOG.info("Deck is ended. Shuffling a new one.");
            deck = Deck.createDeck(DECK_TYPE, DECK_COUNT);
        }
        return deck.popNextCard();
    }
}
