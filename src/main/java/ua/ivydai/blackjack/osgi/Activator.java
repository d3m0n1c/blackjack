package ua.ivydai.blackjack.osgi;

import java.util.Hashtable;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorSystem;
import akka.osgi.ActorSystemActivator;

/*******************************************************************************
 * OSGi activator.
 * 
 * @author IVydai
 ******************************************************************************/
public class Activator extends ActorSystemActivator {
    public static final Logger LOG = LoggerFactory.getLogger(Activator.class);

    public static final String SYSTEM_NAME = "Blackjack";

    private ServiceRegistration<?> service;

    @Override
    public String getActorSystemName(BundleContext context) {
        return SYSTEM_NAME;
    }

    @Override
    public void configure(BundleContext context, ActorSystem system) {
        InstantiateDealer dealerBean = new InstantiateDealerImpl(system);
        if (dealerBean.instantiate(SYSTEM_NAME, "dealerActor")) {
            service = context.registerService(
                    InstantiateDealer.class.getName(), dealerBean,
                    new Hashtable<String, Object>());
        }
    }

    @Override
    public void stop(BundleContext context) {
        if (service != null) {
            context.ungetService(service.getReference());
        }
        super.stop(context);
    }
}