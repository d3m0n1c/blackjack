package ua.ivydai.blackjack.message.player;

import java.io.Serializable;

/*******************************************************************************
 * Message that signalizes about player is out of money or want to leave.
 *
 * @author IVydai
 ******************************************************************************/
public class LeaveTable implements Serializable {
    private static final long serialVersionUID = -6170362130024375871L;
}
