package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message from Dealer to Player about its reference is no more in use and it
 * correctly disconnected.
 *
 * @author IVydai
 ******************************************************************************/
public class OkBye implements Serializable {
    private static final long serialVersionUID = -4336525614948230608L;
}
