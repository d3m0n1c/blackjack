package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message to Player that signalizes about round start and dealer is waiting for
 * bids from all players.
 *
 * @author IVydai
 ******************************************************************************/
public class RoundStarted implements Serializable {
    private static final long serialVersionUID = -612386718028512471L;

    private final int playerMoney;

    private final int bidFromPreviousRounds;

    /***************************************************************************
     * Creates a message to Player about one's money and saved bid from previous
     * tie round.
     * 
     * @param playerMoney - player money for making a bid
     * @param playerBid - saved bid from previous tie round
     */
    public RoundStarted(int playerMoney, int playerBid) {
        this.playerMoney = playerMoney;
        this.bidFromPreviousRounds = playerBid;
    }

    public int getPlayerMoney() {
        return playerMoney;
    }

    public int getPlayerBid() {
        return bidFromPreviousRounds;
    }
}
