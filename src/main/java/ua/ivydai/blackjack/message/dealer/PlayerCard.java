package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

import ua.ivydai.blackjack.model.Card;

/*******************************************************************************
 * Message from Dealer with card for player.
 *
 * @author IVydai
 ******************************************************************************/
public class PlayerCard implements Serializable {
    private static final long serialVersionUID = 1198649686463580706L;

    private final Card card;

    public PlayerCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
