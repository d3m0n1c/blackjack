package ua.ivydai.blackjack.message.player;

import java.io.Serializable;

/*******************************************************************************
 * Message to the Dealer that signalizes that Player need one more card.
 * 
 * @author IVydai
 ******************************************************************************/
public class Hit implements Serializable {
    private static final long serialVersionUID = 4916491971112393186L;
}
