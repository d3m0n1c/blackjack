Simplified version of Blackjack game written on Java, Akka, Hazelcast and Apache Felix.

Maven commands to run or build archive:

1) Maven run:

 - run "mvn clean compile exec:exec"  in folder with pom.xml

OR

 - run eclipse maven task with goal "clean compile exec:exec"

2) Maven zip file build:

 - run "mvn clean install" in folder with pom.xml

OR

 - run eclipse maven task with goal "clean install"

THEN

 - take zip file from target folder