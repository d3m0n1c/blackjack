package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message that signalizes about player's turn began. Dealer waits for Hit and
 * Stands messages.
 *
 * @author IVydai
 ******************************************************************************/
public class YourTurn implements Serializable {
    private static final long serialVersionUID = 3908930652477290753L;
}
