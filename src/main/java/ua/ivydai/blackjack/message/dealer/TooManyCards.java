package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message to the Player that signalizes about too big value in ones hand. 
 * 
 * @author IVydai
 ******************************************************************************/
public class TooManyCards implements Serializable {
    private static final long serialVersionUID = 1830607457957907477L;
}
