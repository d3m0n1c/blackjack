package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

import ua.ivydai.blackjack.model.Hand;

/*******************************************************************************
 * Message from Dealer with dealers hand that loose to player's hand by value.
 *
 * @author IVydai
 ******************************************************************************/
public class Win implements Serializable {
    private static final long serialVersionUID = -1989630641734671742L;

    private final Hand dealerHand;

    public Win(Hand dealerHand) {
        this.dealerHand = dealerHand;
    }

    public Hand getDealerHand() {
        return dealerHand;
    }
}
