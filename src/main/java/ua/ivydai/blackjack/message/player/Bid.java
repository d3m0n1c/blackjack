package ua.ivydai.blackjack.message.player;

import java.io.Serializable;

/*******************************************************************************
 * Message for Dealer that contains bid value for current round.
 *
 * @author IVydai
 ******************************************************************************/
public class Bid implements Serializable {
    private static final long serialVersionUID = 8022695663904006871L;

    private final int playerBid;

    public Bid(int playerBid) {
        this.playerBid = playerBid;
    }

    public int getPlayerBid() {
        return playerBid;
    }
}
