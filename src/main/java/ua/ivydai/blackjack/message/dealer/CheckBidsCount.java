package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Dealer to dealer message to check are all players placed their bids. Also can
 * be called for leaved players to check are all except them made their bids.
 *
 * @author IVydai
 ******************************************************************************/
public class CheckBidsCount implements Serializable {
    private static final long serialVersionUID = 6036144119946225936L;
}
