package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

import ua.ivydai.blackjack.model.Hand;

/*******************************************************************************
 * Message from Dealer with dealers hand that beats player's hand by value.
 *
 * @author IVydai
 ******************************************************************************/
public class Loose implements Serializable {
    private static final long serialVersionUID = 2586281107107676505L;

    private final Hand dealerHand;

    public Loose(Hand dealerHand) {
        this.dealerHand = dealerHand;
    }

    public Hand getDealerHand() {
        return dealerHand;
    }
}
