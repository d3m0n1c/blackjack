package ua.ivydai.blackjack.message.player;

import java.io.Serializable;

/*******************************************************************************
 * Message to the Dealer that signalizes that Player is joined.
 *
 * @author IVydai
 ******************************************************************************/
public class Join implements Serializable {
    private static final long serialVersionUID = -1214322341388732113L;
}
