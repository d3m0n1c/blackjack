package ua.ivydai.blackjack.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/*******************************************************************************
 * Deck that can be created using static createDeck method. Can be created with
 * deck count and type. Shuffled only once on creation.
 * 
 * @author IVydai
 ******************************************************************************/
public class Deck {
    private final List<Card> deckCards;

    private Deck(DeckType deckType, int deckCount) {
        this.deckCards = new LinkedList<Card>();
        for (int i = 0; i < deckCount; i++) {
            deckCards.addAll(deckType.getCards());
        }
        Collections.shuffle(deckCards);
    }

    /***************************************************************************
     * Builds a shuffled deck of required size.
     * 
     * @param deckType
     *            - deck with different set of cards
     * @param deckCount
     *            - count of decks that should be shuffled together
     * @return shuffled deck of required size
     * @throws IllegalArgumentException
     *             deckType should be not null and deckCount should be more than
     *             zero
     */
    public static Deck createDeck(DeckType deckType, int deckCount) {
        if (deckType == null)
            throw new IllegalArgumentException("Deck type can't be null");
        if (deckCount < 1)
            throw new IllegalArgumentException(
                    "Deck count should be more than zero");
        return new Deck(deckType, deckCount);
    }

    /***************************************************************************
     * Pops a next card.
     * 
     * @return next playing card
     * @throws IllegalStateException no more cards in the deck
     */
    public Card popNextCard() {
        if (deckCards.isEmpty())
            throw new IllegalStateException("No more cards in the deck");
        return deckCards.remove(0);
    }

    /***************************************************************************
     * Gets current size of the deck.
     * 
     * @return current size of the deck
     */
    public int size() {
        return deckCards.size();
    }

    @Override
    public String toString() {
        if (deckCards.isEmpty()) {
            return "Empty deck";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Deck of ").append(deckCards.size())
                .append(" card(s), top card is ").append(deckCards.get(0));
        return sb.toString();
    }
}
