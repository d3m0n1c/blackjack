package ua.ivydai.blackjack.message.player;

import java.io.Serializable;

/*******************************************************************************
 * Message to the Dealer that signalizes that Player stops taking cards.
 * 
 * @author IVydai
 ******************************************************************************/
public class Stand implements Serializable {
    private static final long serialVersionUID = -2656749091783882443L;
}
