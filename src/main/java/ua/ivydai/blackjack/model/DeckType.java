package ua.ivydai.blackjack.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*******************************************************************************
 * Playing cards deck type with different suits and values ranges. 
 * 
 * @author IVydai
 ******************************************************************************/
public enum DeckType {
    STANDARD_DECK(CardValue.values(), CardSuit.values());

    private final List<Card> deckCards;

    private DeckType(CardValue[] values, CardSuit[] suits) {
        assert(values != null && values.length > 0);
        assert(suits != null && suits.length > 0);
        
        List<Card> cards = new ArrayList<Card>(values.length * suits.length);
        for (CardValue value : values) {
            for (CardSuit suit : suits) {
                cards.add(new Card(suit, value));
            }
        }
        deckCards = Collections.unmodifiableList(cards);
    }

    /***************************************************************************
     * Gets unmodifiable list of deck cards.
     * 
     * @return unmodifiable list of deck cards
     */
    public List<Card> getCards() {
        return deckCards;
    }

    /***************************************************************************
     * Gets deck size.
     * 
     * @return deck size
     */
    public int size() {
        return deckCards.size();
    }
}
