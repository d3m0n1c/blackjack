title Blackjack game
Player->Dealer: 'Join'
    Dealer->Dealer: 'NextRound'
opt If game is not InProgress and players count >= min_players_limit
    Dealer->Player: 'RoundStarted'
end
alt Player typed 'quit'
    Player->Dealer: 'LeaveTable'
    Dealer->Player: 'OkBye'
else Player pick correct bid value
    Player->Dealer: 'Bid'
end
Dealer->Dealer: 'CheckBidCount' on each LeaveTable or Bid
Dealer->Player: 'PlayerCard' for each player
Dealer->Player: 'DealerCard' for each player
Dealer->Player: 'PlayerCard' for each player
loop For each player in order of max hand value
    Player->Dealer: Hit
    alt Hand value < 21
        Dealer->Player: 'PlayerCard'
        Dealer->Player: 'YourTurn'
    else Hand value 21
        Dealer->Player: 'Blackjack'
    else Hand value > 21
        Dealer->Player: 'TooManyCards'
    end
    Player->Dealer: 'Stand'
end
Dealer->Dealer: 'DealerTurn'
alt Dealer hand lower than Player hand
    Dealer->Player: 'Win'
else Dealer hand more than Player hand
    Dealer->Player: 'Loose'
    opt If player has no more money
        Dealer->Player: 'NoMoreMoney'
        Dealer->Dealer: 'LeaveTable'
        Dealer->Player: 'OkBye'
    end
else Dealer hand equals to Player hand
    Dealer->Player: 'Tie'
end
Dealer->Dealer: 'NextRound'

http://www.websequencediagrams.com/