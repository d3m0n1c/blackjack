package ua.ivydai.blackjack;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Random;

import org.jboss.netty.channel.ChannelException;

import scala.Console;
import ua.ivydai.blackjack.actor.PlayerActor;
import ua.ivydai.blackjack.message.player.Join;

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

/*******************************************************************************
 * Client starter for blackjack player.
 *
 * @author IVydai
 ******************************************************************************/
public class PlayerStarter {
    private static final String DEALER_URI = "akka.tcp://Blackjack@127.0.0.1:2553/user/dealerActor";

    private static final String DEFAULT_CLIENT_PORT = "12345";

    public static void main(String[] args) {
        String uid = BigInteger.probablePrime(64, new Random()).toString(36);
        boolean portNotAvailable = true;
        String port = null;
        do {
            System.out.printf("Please enter client port <default is %s>: ",
                    DEFAULT_CLIENT_PORT);
            port = Console.readLine().trim();
            if (port.isEmpty()) {
                port = DEFAULT_CLIENT_PORT;
            }
            portNotAvailable = !available(port);
            if (portNotAvailable) {
                System.out.println("Port is wrong or already busy");
            }
        } while (portNotAvailable);
        
        StringBuilder configStringBuilder = new StringBuilder();
        configStringBuilder.append("akka {\n");
        configStringBuilder.append("  actor {\n");
        configStringBuilder.append("    provider = \"akka.remote.RemoteActorRefProvider\"\n");
        configStringBuilder.append("  }\n");
        configStringBuilder.append("  remote {\n");
        configStringBuilder.append("    transport = [\"akka.remote.netty.tcp\"]\n");
        configStringBuilder.append("    netty.tcp {\n");
        configStringBuilder.append("      hostname = \"localhost\"\n");
        configStringBuilder.append("      port = ").append(port).append('\n');
        configStringBuilder.append("    }\n");
        configStringBuilder.append("  }\n");
        configStringBuilder.append("}");
        ActorSystem system;
        try {
            system = ActorSystem.create(uid,
                    ConfigFactory.parseString(configStringBuilder.toString()));
            ActorRef playerRef = system.actorOf(
                    Props.create(PlayerActor.class, DEALER_URI), uid);
            playerRef.tell(new Join(), null);
        } catch (ChannelException e) {
            e.printStackTrace();
        }
    }

    /***************************************************************************
     * Checks to see if a specific port is available.
     * This is the implementation coming from the Apache camel project.
     *
     * @param portAsText - the port to check for availability
     */
    public static boolean available(String portAsText) {
        Integer port = null;
        try {
            port = Integer.parseInt(portAsText);
        } catch (NumberFormatException e) {
            /* should not be thrown */
        }
        if (port == null) return false;
        if (port < 1100 || port > 49151) {
            return false;
        }
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
            /* should not be thrown */
        } finally {
            if (ds != null) {
                ds.close();
            }
            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }
        return false;
    }
}
