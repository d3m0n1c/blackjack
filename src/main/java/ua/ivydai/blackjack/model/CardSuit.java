package ua.ivydai.blackjack.model;

/*******************************************************************************
 * Card suits.
 * 
 * @author IVydai
 ******************************************************************************/
public enum CardSuit {
    SPADE(Character.toChars(9824)[0]),
    HEART(Character.toChars(9829)[0]),
    DIAMOND(Character.toChars(9830)[0]),
    CLUB(Character.toChars(9827)[0]);

    private final char charRepresentation;

    private CardSuit(char charRepresentation) {
        this.charRepresentation = charRepresentation;
    }

    public char getCharRepresentation() {
        return charRepresentation;
    }
}
