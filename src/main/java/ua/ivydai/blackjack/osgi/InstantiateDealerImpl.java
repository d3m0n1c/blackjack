package ua.ivydai.blackjack.osgi;

import ua.ivydai.blackjack.actor.DealerActor;
import akka.actor.ActorSystem;
import akka.actor.Props;

/*******************************************************************************
 * Entry point to bundle activation implementation for DealerActor start. 
 * 
 * @author IVydai
 ******************************************************************************/
public class InstantiateDealerImpl implements InstantiateDealer {
    private ActorSystem system;

    public InstantiateDealerImpl(ActorSystem system) {
        this.system = system;
    }

    @Override
    public boolean instantiate(String actorSystemName, String dealerID) {
        try {
            system.actorOf(Props.create(DealerActor.class), dealerID);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
