package ua.ivydai.blackjack.model;

import java.io.Serializable;

/*******************************************************************************
 * Playing card that consists of suit and value.
 * 
 * @author IVydai
 ******************************************************************************/
public class Card implements Serializable {
    private static final long serialVersionUID = 4556658259739288123L;

    private final CardSuit suit;

    private final CardValue value;

    /***************************************************************************
     * Creates a card that consists of suit and value.
     * 
     * @param suit
     *            - card suit
     * @param value
     *            - card value
     */
    public Card(CardSuit suit, CardValue value) {
        this.suit = suit;
        this.value = value;
    }

    public CardSuit getCardSuit() {
        return suit;
    }

    public CardValue getCardValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((suit == null) ? 0 : suit.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Card other = (Card) obj;
        if (suit != other.suit)
            return false;
        if (value != other.value)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "[" + value.getStringRepresentation()
                + suit.getCharRepresentation() + "]";
    }
}
