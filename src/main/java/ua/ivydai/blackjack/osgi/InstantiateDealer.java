package ua.ivydai.blackjack.osgi;

/*******************************************************************************
 * Entry point for bundle activation.
 * 
 * @author IVydai
 ******************************************************************************/
public interface InstantiateDealer {
    boolean instantiate(String actorSystemName, String dealerID);
}