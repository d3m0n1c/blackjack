package ua.ivydai.blackjack.actor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.Console;
import ua.ivydai.blackjack.message.dealer.*;
import ua.ivydai.blackjack.message.player.*;
import ua.ivydai.blackjack.model.Hand;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.UntypedActor;

/*******************************************************************************
 * Player actor is a client side. Knows only about dealer and acts only on
 * dealer's demand.
 *
 * @author IVydai
 ******************************************************************************/
public class PlayerActor extends UntypedActor {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerActor.class);

    private static final String BID_WARNING = "Wrong input. Please enter a correct number or type word 'quit'";

    private final ActorSelection dealer;

    private Hand playerHand;

    public PlayerActor(String dealerUri) {
        dealer = getContext().actorSelection(dealerUri);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        final ActorRef self = getSelf();
        if (message instanceof Join) {
            dealer.tell(message, self);
        } else if (message instanceof RoundStarted) {
            playerHand = new Hand();
            RoundStarted input = (RoundStarted) message;
            System.out.println("------------<[ NEW ROUND ]>------------");
            System.out.printf("You have %d coins and %d already bidded coins\n",
                    input.getPlayerMoney(), input.getPlayerBid());
            boolean isNotCorrectBid = true;
            int playerBid = -1;
            do {
                System.out.print("Place your bid (or type 'quit' to left the game): ");
                String playerBidText = Console.readLine().trim();
                if ("quit".equalsIgnoreCase(playerBidText)) {
                    dealer.tell(new LeaveTable(), self);
                    return;
                }
                try {
                    playerBid = Integer.parseInt(playerBidText);
                    if (playerBid > 0 && playerBid <= input.getPlayerMoney()
                            || playerBid == 0 && input.getPlayerBid() > 0) {
                        isNotCorrectBid = false;
                    } else {
                        System.out.println(BID_WARNING);
                    }
                } catch (NumberFormatException e) {
                    System.out.println(BID_WARNING);
                }
            } while (isNotCorrectBid);
            dealer.tell(new Bid(playerBid), self);
        } else if (message instanceof NoMoreMoney) {
            System.out.println("No money, no funny!");
        } else if (message instanceof OkBye) {
            System.out.println("Bye bye!");
            System.exit(0);
        } else if (message instanceof PlayerCard) {
            PlayerCard card = (PlayerCard) message;
            playerHand.addOpenedCard(card.getCard());
            System.out.println("Dealer gives you a card: " + card.getCard());
        } else if (message instanceof DealerCard) {
            DealerCard card = (DealerCard) message;
            System.out.println("Dealer takes a card: " + card.getCard());
        } else if (message instanceof YourTurn) {
            System.out.println("Your hand is " + playerHand);
            int action = -1;
            while (action < 1 || action > 2) {
                System.out.print("Choose your action(1: HIT, 2: STAND): ");
                action = Console.readInt();
            }
            switch (action) {
                case 1: dealer.tell(new Hit(), self); break;
                case 2: dealer.tell(new Stand(), self); break;
            }
        } else if (message instanceof Blackjack) {
            System.out.println("Blackjack! Your hand: " + playerHand);
        } else if (message instanceof TooManyCards) {
            System.out.println("Too many cards. Your hand: " + playerHand);
        } else if (message instanceof Loose) {
            Loose input = (Loose) message;
            System.out.println("FAIL! Dealer's hand: " + input.getDealerHand());
        } else if (message instanceof Win) {
            Win input = (Win) message;
            System.out.println("WIN! Dealer's hand: " + input.getDealerHand());
        } else if (message instanceof Tie) {
            Tie input = (Tie) message;
            System.out.println("TIE! Dealer's hand: " + input.getDealerHand());
        } else {
            LOG.warn("Unhandled message: " + message.toString()); // TODO
            unhandled(message);
        }
    }
}
