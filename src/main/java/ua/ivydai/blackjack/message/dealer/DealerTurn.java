package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message to Dealer from Dealer to make dealers turns and send results to each
 * player.
 *
 * @author IVydai
 ******************************************************************************/
public class DealerTurn implements Serializable {
    private static final long serialVersionUID = -1292421394825941266L;
}
