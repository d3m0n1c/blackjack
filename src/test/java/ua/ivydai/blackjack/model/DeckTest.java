package ua.ivydai.blackjack.model;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestSuite;

/*******************************************************************************
 * Test for Deck class.
 * 
 * @author IVydai
 ******************************************************************************/
public class DeckTest extends TestSuite {
    @Test
    public void testDeckCreation() {
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, 1);
        assertEquals(deck.size(), 52);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeDeckCreationDeckCount() {
        Deck.createDeck(DeckType.STANDARD_DECK, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeDeckCreationDeckType() {
        Deck.createDeck(null, 1);
    }

    @Test
    public void testDeckSize() {
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, 1);
        deck.popNextCard();
        assertEquals(deck.size(), 51);
    }

    @Test(timeout = 100, expected = IllegalStateException.class)
    public void testNegativeDeckSize() {
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, 1);
        while (true) {
            deck.popNextCard();
        }
    }

    @Test
    public void testDeckCardReusing() {
        Card card = Deck.createDeck(DeckType.STANDARD_DECK, 1).popNextCard();
        Card sameCard = null;
        Deck deck = Deck.createDeck(DeckType.STANDARD_DECK, 1);
        while (deck.size() > 0) {
            Card nextCard = deck.popNextCard();
            if (card == nextCard) { // yes, yes... check by reference
                sameCard = nextCard;
                break;
            }
        }
        assertNotNull(sameCard);
    }
}
