package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message from Delaer to Dealer for starting a round.
 *
 * @author IVydai
 ******************************************************************************/
public class StartRound implements Serializable {
    private static final long serialVersionUID = 1832264759915662560L;
}
