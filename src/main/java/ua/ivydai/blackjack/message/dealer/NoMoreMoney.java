package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Dealer send this message to Player if player Loose and don't have more money.
 *
 * @author IVydai
 ******************************************************************************/
public class NoMoreMoney implements Serializable {
    private static final long serialVersionUID = 7608090625877471734L;
}
