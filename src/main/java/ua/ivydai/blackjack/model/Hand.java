package ua.ivydai.blackjack.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/*******************************************************************************
 * Hand with cards. Cards can be opened and closed.
 * 
 * @author IVydai
 ******************************************************************************/
public class Hand implements Serializable {
    private static final long serialVersionUID = -1614457539197613793L;

    private final List<Card> openedCards = new ArrayList<Card>();

    private final List<Card> closedCards = new LinkedList<Card>();

    public void addOpenedCard(Card card) {
        openedCards.add(card);
    }

    public int getOpenedCardsCount() {
        return openedCards.size();
    }

    /***************************************************************************
     * Gets unmodifiable list of opened cards.
     * 
     * @return unmodifiable list of opened cards
     */
    public List<Card> getOpenedCards() {
        return Collections.unmodifiableList(openedCards);
    }

    public void addClosedCard(Card card) {
        closedCards.add(card);
    }

    public int getClosedCardsCount() {
        return closedCards.size();
    }

    /***************************************************************************
     * Opens next closed card.
     *
     * @throws IllegalStateException no more closed cards to open
     */
    public void openNextClosedCard() {
        if (closedCards.isEmpty()) {
            throw new IllegalStateException("No more closed cards to open");
        }
        openedCards.add(closedCards.remove(0));
    }

    public int getHandValue() {
        int result = 0;
        for (Card card : openedCards) {
            result += card.getCardValue().getValue();
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Opened(");
        if (openedCards.isEmpty()) {
            sb.append("<empty>");
        } else {
            for (Card card : openedCards) {
                sb.append(card);
            }
        }
        sb.append(") and Closed(");
        if (closedCards.isEmpty()) {
            sb.append("<empty>");
        } else {
            for (Card card : closedCards) {
                sb.append(card);
            }
        }
        sb.append(") cards with value ").append(getHandValue());
        return sb.toString();
    }
}
