package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message from Dealer to Dealer for making a turn cycle.
 *
 * @author IVydai
 ******************************************************************************/
public class NextPlayerTurn implements Serializable {
    private static final long serialVersionUID = -2621592833575308649L;
}
