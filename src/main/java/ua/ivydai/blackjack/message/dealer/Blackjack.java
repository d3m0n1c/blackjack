package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

/*******************************************************************************
 * Message from Dealer to Player about Blackjack. 
 *
 * @author IVydai
 ******************************************************************************/
public class Blackjack  implements Serializable {
    private static final long serialVersionUID = 3674070399075381170L;
}
