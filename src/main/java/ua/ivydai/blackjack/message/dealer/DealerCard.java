package ua.ivydai.blackjack.message.dealer;

import java.io.Serializable;

import ua.ivydai.blackjack.model.Card;

/*******************************************************************************
 * Message from Dealer with a card that he took in its hand.
 *
 * @author IVydai
 ******************************************************************************/
public class DealerCard  implements Serializable {
    private static final long serialVersionUID = -6246416197693145484L;

    private final Card card;

    public DealerCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
